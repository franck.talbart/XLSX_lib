XLSX_lib
========

XLSX_lib is a python library used to inject data into XLSX files (default Excel 2007 workbook format). Doing so, a user may create his own XLSX templates
with formulas and charts, and populate them. This approach allows to create all types of charts and formulas without any limitation.
The library is able to insert both text and number values. On top of that, it automatically removes the
pre-calculated values. Doing so, Excel must compute the formula with the new inserted values.

The library is able to manage a set of worksheets.

The produced files are supported by Excel 2007 and above, and Open Office.

DEMO
====

Simply run "python ./demo.py".

LICENSE
=======

This library is released under the GNU General Public License, version 3. Please refer to LICENSE, for details.

CONTACT
=======

For any question regarding XLSX_lib, please write at <franck@talbart.fr>.

DOCUMENTATION
=============
    def __init__(self, filename):
        Constructor used to prepare the XLSX file. Default selected worksheet if the 1st one.
    def select_worksheet(self, worksheet):
        Select the worksheet to update (by default, 1st worksheet is selected). The worksheet could be "sheet2", "sheet3", etc...
    def insert_data(self, column, line, value):
        Insert data into the worksheet
    def save_worksheet(self):
        Record the updated worksheet. THEN, THE XLSX FILE MUST BE GENERATED
    def build(self, filename):
        Generate the updated XLSX file
            filename: the filename of the new XLSX file. It is generated in the current directory.

AUTHOR
=======

The author list is written in the AUTHOR file.


